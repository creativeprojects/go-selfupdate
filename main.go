package main

import "fmt"

// These fields are populated by the goreleaser build
var (
	version = "0.0.0-dev"
	commit  = "n/a"
	date    = "now"
	builtBy = "dev"
)

func main() {
	fmt.Printf("go-selfupdate version %s built %s by %s\n", version, date, builtBy)
}
